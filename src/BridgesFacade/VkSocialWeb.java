package BridgesFacade;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Olesya on 04.05.2017.
 */
public class VkSocialWeb extends BasicSocialWeb {
    public String getHistory(){
        return "VKSocialWeb history";
    }

    List<Integer> getLikes(long userId, int messageId) {
        return Arrays.asList(1, 2, 3);
    }

    public String makeDossier() {
        return "все чисто";
    }

    public String paymentDetailing() {
        return "налоги платит";
    }
}
