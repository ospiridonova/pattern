package builder.galustan;

import builder.HouseBuilder;

/**
 * Created by Olesya on 03.05.2017.
 */
public class Jamshut implements HouseBuilder {
    private String basement;
    private String walls;
    private String roof;


    @Override
    public void fillBase() {
        basement = "Done basement by Ravshan";
    }

    @Override
    public void createWalls() {
        walls = "Done walls by Jamshut";
    }

    @Override
    public void createRoof() {
        roof = "Крыша by Jamshut";
    }

    @Override
    public String getResult() {
        return basement + " " + walls + " " + roof;
    }
}
