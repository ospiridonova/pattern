package strategy;

/**
 * Created by Olesya on 05.05.2017.
 */
public interface HolidayStrategy {

    void selebrate();
}
