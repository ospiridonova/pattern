package strategy;

/**
 * Created by Olesya on 05.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Context context = new Context();
        context.setHasFamily(true);
        context.setHasFriends(true);
        context.setCash(8000);
        context.selebrate();
    }
}
