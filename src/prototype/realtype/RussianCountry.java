package prototype.realtype;

import prototype.proto.Country;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Olesya on 03.05.2017.
 */
public class RussianCountry extends Country {

    private List<RussianHouse> houses;
    private List<RussianDistrict> districts;
    private List<RussianCity> cities;

    @Override
    public RussianCountry clone() throws CloneNotSupportedException {
        RussianCountry copy = (RussianCountry) super.clone();
        houses = createHouses();
        districts = createDistricts();
        cities = createCities();
        return copy;
    }

    public List<RussianHouse> createHouses() {
        List<RussianHouse> houses = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            houses.add(new RussianHouse());
        }
        return houses;
    }

    public List<RussianDistrict> createDistricts() {
        List<RussianDistrict> districts = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            districts.add(new RussianDistrict());
        }
        return districts;
    }

    public List<RussianCity> createCities() {
        List<RussianCity> cities = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            cities.add(new RussianCity());
        }
        return cities;
    }

}
