package adapter;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Olesya on 04.05.2017.
 */
public class PostgreDb implements DB{
    @Override
    public List<String> getListForFriends() {
        return Arrays.asList("postgre Artem", "posgtre Alex");
    }

    @Override
    public int getMoney() {
        return 0;
    }

    @Override
    public List<String> getWall() {
        return Arrays.asList("postgre Artem hi ", "postgreAlex hi");
    }
}
