package adapter;

import java.util.List;

/**
 * Created by Olesya on 04.05.2017.
 */
public interface DB {

    List<String> getListForFriends();

    int getMoney();

    List<String> getWall();
}
