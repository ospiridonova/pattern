package adapter;

import java.util.List;

/**
 * Created by Olesya on 04.05.2017.
 */
public class ExtendedSocialWebAdapter implements ExtendedSocialWeb {

    private BasicSocialWeb bw;

    public ExtendedSocialWebAdapter(BasicSocialWeb bw) {
        this.bw = bw;
    }

    @Override
    public String getHistory(int userId, String date) {
        if(bw instanceof VKSocialWeb){
            return new VKSocialWeb().getHistory();
        } else if(bw instanceof FaceBook) {
            return new FaceBook().getHistory("date");
        }
        return null;
    }

    @Override
    public List<Integer> getLikes(int userId, boolean showNegative, int messageId) {
        return null;
    }
}
