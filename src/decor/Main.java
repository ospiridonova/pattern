package decor;

/**
 * Created by Olesya on 04.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        MainTariff mainTariff = new MainTariff();
        RoumingTariff roumingTariff = new RoumingTariff(mainTariff);
        SmsTariff smsTariff = new SmsTariff(roumingTariff);
        SmsTariff internetTariff = new SmsTariff(smsTariff);
        internetTariff.processTarif();
    }
}
