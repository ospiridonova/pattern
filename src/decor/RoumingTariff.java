package decor;

/**
 * Created by Olesya on 04.05.2017.
 */
public class RoumingTariff extends TariffDecorator {

    public RoumingTariff(CountTariffInterface decorator) {
        super(decorator);
    }

    @Override
    public void processTarif() {
        System.out.println("100 min in Roming");
        super.processTarif();
    }
}
