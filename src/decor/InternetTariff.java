package decor;

/**
 * Created by Olesya on 04.05.2017.
 */
public class InternetTariff extends TariffDecorator {

    public InternetTariff(CountTariffInterface decorator) {
        super(decorator);
    }

    @Override
    public void processTarif() {
        System.out.println("100 Gb in Roming");
        super.processTarif();
    }
}
