package factory.berreta;

import factory.*;

/**
 * Created by Olesya on 03.05.2017.
 */
public class BerretaFactory implements WeaponFactory {
    @Override
    public Gun createGun() {
        return new BerretaGun();
    }

    @Override
    public Revolver createRevolver() {
        return new BerretaRevolver();
    }

    @Override
    public Rifle createRifle() {
        return new BerretaRifle();
    }

    @Override
    public BFG createBFG() {
        return new BerretaBFG();
    }
}
