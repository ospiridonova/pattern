package factory.colt;

import factory.*;

/**
 * Created by Olesya on 03.05.2017.
 */
public class ColtFactory implements WeaponFactory {
    @Override
    public Gun createGun() {
        return new ColtGun();
    }

    @Override
    public Revolver createRevolver() {
        return new ColtRevolver();
    }

    @Override
    public Rifle createRifle() {
        return new ColtRifle();
    }

    @Override
    public BFG createBFG() {
        return new ColtBFG();
    }
}
