package facade;

/**
 * Created by Olesya on 04.05.2017.
 */
public interface Inspector {

    void getPaysFromVk();

    void getPaysFromFb();
}
