package facade;

/**
 * Created by Olesya on 04.05.2017.
 */
public interface Detective {

    void makeBrief();
}
