package observer;

/**
 * Created by Olesya on 04.05.2017.
 */
public interface Observer {

    void message(String message);
}
