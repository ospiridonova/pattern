package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class StateNew extends State {
    /**
     * Контекст передаёт себя в конструктор состояния, чтобы состояние могло
     * обращаться к его данным и методам в будущем, если потребуется.
     *
     * @param document
     */
    public StateNew(Document document) {
        super(document);
    }

    @Override
    public void onNext() {
        document.changeState(new StateSending(document));
        document.setDo(false);
        System.out.println("New state");
    }
}
