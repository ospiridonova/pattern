package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class StateSigned extends State {

    /**
     * Контекст передаёт себя в конструктор состояния, чтобы состояние могло
     * обращаться к его данным и методам в будущем, если потребуется.
     *
     * @param document
     */
    public StateSigned(Document document) {
        super(document);
    }

    @Override
    public void onNext() {
        if (EDO.sending()) {
            document.changeState(new StateRegistered(document));
            System.out.println("Signed state");
        } else {
            document.changeState(new StateSignError(document));
            System.out.println("Locked...");
        }

    }
}
