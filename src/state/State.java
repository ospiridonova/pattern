package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public abstract class State {
    public Document document;

    public State(Document document) {
        this.document = document;
    }

    /**
     * Контекст передаёт себя в конструктор состояния, чтобы состояние могло
     * обращаться к его данным и методам в будущем, если потребуется.
     */


    public abstract void onNext();
}
