package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class Document {
    private State state;
    private boolean isDo = true;

    public void changeState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public boolean isDo() {
        return isDo;
    }

    public void setDo(boolean aDo) {
        isDo = aDo;
    }
}