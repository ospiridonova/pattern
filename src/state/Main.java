package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class Main {
    public static void main(String[] args) {
        Document document = new Document();
        document.changeState(new StateNew(document));
        document.getState().onNext();
        document.getState().onNext();
        document.getState().onNext();
    }
}
