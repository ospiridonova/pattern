package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class StateDelivered extends State {

    /**
     * Контекст передаёт себя в конструктор состояния, чтобы состояние могло
     * обращаться к его данным и методам в будущем, если потребуется.
     *
     * @param document
     */
    public StateDelivered(Document document) {
        super(document);
    }

    @Override
    public void onNext() {
        if (EDO.sended()) {
            document.changeState(new StateSended(document));
            document.setDo(false);
            System.out.println("Delivered state");
        } else {
            System.out.println("Locked...");
        }

    }
}
