package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class StateSignError extends State {

    /**
     * Контекст передаёт себя в конструктор состояния, чтобы состояние могло
     * обращаться к его данным и методам в будущем, если потребуется.
     *
     * @param document
     */
    public StateSignError(Document document) {
        super(document);
    }

    @Override
    public void onNext() {
        if (document.isDo()) {
            document.changeState(new StateNew(document));
            document.setDo(false);
            System.out.println("Sign error state");
        } else {
            System.out.println("Locked...");
        }

    }
}
