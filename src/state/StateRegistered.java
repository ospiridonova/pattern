package state;

/**
 * Created by Olesya on 05.05.2017.
 */
public class StateRegistered extends State {

    /**
     * Контекст передаёт себя в конструктор состояния, чтобы состояние могло
     * обращаться к его данным и методам в будущем, если потребуется.
     *
     * @param document
     */
    public StateRegistered(Document document) {
        super(document);
    }

    @Override
    public void onNext() {
        if (EDO.registered()) {
            document.changeState(new StateSended(document));
            document.setDo(false);
            System.out.println("Registered state");
        } else {
            System.out.println("Locked...");
        }

    }
}
