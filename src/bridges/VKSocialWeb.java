package bridges;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Olesya on 04.05.2017.
 */
public class VKSocialWeb extends BasicSocialWeb {

    public String getHistory() {
        return "VKSocialWeb History";
    }

    public List<Integer> getLikes(int userId, int messageId){
        return Arrays.asList(1, 2);
    }
}
