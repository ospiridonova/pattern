package bridges;

import java.util.List;

/**
 * Created by Olesya on 04.05.2017.
 */
public class BasicSocialWeb implements SocialWeb {

    private DB postgeDB = new PostgreDb();

    //List<String> friends = Arrays.asList("Artem", "Alex");
    //List<String> notifications = Arrays.asList("Artem hi ", "Alex hi");

    private int money = 0;

    @Override
    public List<String> getFriends() {
        return postgeDB.getListForFriends();
    }

    @Override
    public void pay(int value) {
        //money += value;
        money += postgeDB.getMoney() + value;
    }

    @Override
    public List<String> wall() {
        return postgeDB.getWall();
    }
}
