package builder;

/**
 * Created by Olesya on 03.05.2017.
 */
public interface HouseBuilder {
    void fillBase();
    void createWalls();
    void createRoof();
    String getResult();
}
